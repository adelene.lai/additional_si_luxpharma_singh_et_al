#load relevant libraries
library(RChemMass,quietly=TRUE)
library(plyr,quietly=TRUE)
library(dplyr,quietly=TRUE)
library(readr,quietly=TRUE)
library(ggplot2,quietly=TRUE)
library(rcdk,quietly=TRUE)
library(magick,quietly=TRUE)
library(purrr,quietly=TRUE)
library(gridExtra,quietly=TRUE)
source("extractAnnotations.R")

image_read("cns_screenshot.png")

#read in CIDs list (output from IDExchange)
idex_cns_cids <- read.table("input/1234827526505398810_816_synonym_cns_input_cids.txt",sep="\t")
colnames(idex_cns_cids) <- c("input_name_CNS","cid")
head(idex_cns_cids)
str(idex_cns_cids)

#how many names and cids are unique?
sapply(idex_cns_cids, function(x) length(unique(x)))

#are there any NAs (compound names for which no CID could be found via ID Exchange Service)
no_cids_avail <- idex_cns_cids[is.na(idex_cns_cids$cid),]
no_cids_avail 

#remove these 6 NAs and deal with them later
query_idex_cns_cids <- idex_cns_cids[!is.na(idex_cns_cids$cid),] 
query_idex_cns_cids <- tibble::rownames_to_column(query_idex_cns_cids, "idx") #add idx column

dim(query_idex_cns_cids)
length(unique(query_idex_cns_cids$input_name_CNS))
length(unique(query_idex_cns_cids$cid))

 query_idex_cns_cids[duplicated(query_idex_cns_cids$cid)|duplicated(query_idex_cns_cids$cid,fromLast=TRUE),]

multiple_cid_per_name_query_idex_cns_cidsquery_idex_cns_cids <- query_idex_cns_cids  %>% dplyr::group_by(input_name_CNS) %>% dplyr::summarise(n = n())

p1 <- ggplot(multiple_cid_per_name_query_idex_cns_cidsquery_idex_cns_cids,aes(n)) + 
geom_bar() + 
scale_y_continuous(breaks=seq(0, 800, 100)) + 
labs(title = "How Many CNS Compounds Have >1 CID? ", x="No. CIDs", y="No. Compounds") +
theme(plot.title = element_text(size=20,hjust=0.5)) + theme(text = element_text(size=16)) 

morethan1_cid_per_name_query_idex_cns_cidsquery_idex_cns_cids <- multiple_cid_per_name_query_idex_cns_cidsquery_idex_cns_cids[multiple_cid_per_name_query_idex_cns_cidsquery_idex_cns_cids$n>1,]
p2 <- ggplot(morethan1_cid_per_name_query_idex_cns_cidsquery_idex_cns_cids,aes(n)) + geom_bar() + scale_y_continuous(breaks=seq(0, 12, 2)) + 
labs(x="No. CIDs", y="No. Compounds") + #title = "How Many CNS Compounds Have >1 CID? ",
theme(plot.title = element_text(size=20,hjust=0.5)) + theme(text = element_text(size=16)) 


g <- ggplotGrob(p2)
p1+ annotation_custom(
    grob = g,
    xmin = 5,
    xmax = 20,
    ymin = 200,
    ymax = 750
  )

morethan1_cid_per_name_query_idex_cns_cidsquery_idex_cns_cids[order(-morethan1_cid_per_name_query_idex_cns_cidsquery_idex_cns_cids$n),]

morethan1_cid_per_name_query_idex_cns_cidsquery_idex_cns_cids$input_name_CNS

multiple_cid_per_name_query_idex_cns_cidsquery_idex_cns_cids[multiple_cid_per_name_query_idex_cns_cidsquery_idex_cns_cids$input_name_CNS=="nicotine",]

multiple_cid_per_name_query_idex_cns_cidsquery_idex_cns_cids[multiple_cid_per_name_query_idex_cns_cidsquery_idex_cns_cids$input_name_CNS=="ketoconazole",]

query_idex_cns_cids[query_idex_cns_cids$input_name_CNS=="ketoconazole",]

multiple_cid_per_name_query_idex_cns_cidsquery_idex_cns_cids[multiple_cid_per_name_query_idex_cns_cidsquery_idex_cns_cids$input_name_CNS=="tramadol",]
#interesting given https://pubchem.ncbi.nlm.nih.gov/compound/63015 and https://pubchem.ncbi.nlm.nih.gov/compound/33741 (33741 has the TPs)

start_n <- 1 #rowname, must be changed acccordingly
chunks <- split(query_idex_cns_cids$idx[start_n:length(query_idex_cns_cids$idx)], ceiling(seq_along(query_idex_cns_cids$idx[start_n:length(query_idex_cns_cids$idx)])/10)) 

start.time <- Sys.time()
n_requests <- 0
n_tps <- list()

for (j in chunks){
  for (i in j){
    print(paste0("Running query idx ", i, " with cid ", query_idex_cns_cids[query_idex_cns_cids$idx==i,"cid"]))
    cids_successors <- getPcCand.trans(query_idex_cns_cids[query_idex_cns_cids$idx==i,"cid"]) 
    n_requests <- n_requests + 1
    print(paste0(n_requests," completed."))
    print(cids_successors)
    if (is.na(cids_successors)) {  
      n_tps[[i]] <- 0
    } else {
      n_tps[[i]] <- length(cids_successors)
    }
  }
  print("pause for 20 seconds ")
  Sys.sleep(20)
}

end.time <- Sys.time()
time.taken <- end.time - start.time
time.taken

start_n <- 491 #rowname
chunks <- split(query_idex_cns_cids$idx[start_n:length(query_idex_cns_cids$idx)], ceiling(seq_along(query_idex_cns_cids$idx[start_n:length(query_idex_cns_cids$idx)])/10)) 

start.time <- Sys.time()
n_requests <- 0
n_tps <- list()

for (j in chunks){
  for (i in j){
    print(paste0("Running query idx ", i, " with cid ", query_idex_cns_cids[query_idex_cns_cids$idx==i,"cid"]))
    cids_successors <- getPcCand.trans(query_idex_cns_cids[query_idex_cns_cids$idx==i,"cid"]) 
    n_requests <- n_requests + 1
    print(paste0(n_requests," completed."))
    print(cids_successors)
    if (is.na(cids_successors)) {  
      n_tps[[i]] <- 0
    } else {
      n_tps[[i]] <- length(cids_successors)
    }
  }
  print("pause for 20 seconds ")
  Sys.sleep(20)
}

end.time <- Sys.time()
time.taken <- end.time - start.time
time.taken

#look into cases where cid was NA
no_cids_avail

#manually extract their CIDs 
no_cids_avail$cid <- c(5702198,135565674,16134627,15684,3086011,43805) #2,4-dcbal

#add rows for dichlorobenzyl alcohol (isomers)
dcbal_isomer1 <-data.frame("dichlorobenzyl alcohol","43236") #3,5
dcbal_isomer2 <-data.frame("dichlorobenzyl alcohol","15728") #3,4
dcbal_isomer3 <-data.frame("dichlorobenzyl alcohol","118604") #2,5
 

names(dcbal_isomer1)<-c("input_name_CNS","cid")  
names(dcbal_isomer2)<-c("input_name_CNS","cid")  
names(dcbal_isomer3)<-c("input_name_CNS","cid")  

rownames(dcbal_isomer1) <- 1269
rownames(dcbal_isomer2) <- 1270
rownames(dcbal_isomer3) <- 1271

no_cids_avail_man_cur <- rbind(no_cids_avail,dcbal_isomer1,dcbal_isomer2,dcbal_isomer3)

no_cids_avail_man_cur

#small list, can query without chunking 
start.time <- Sys.time()
n_requests <- 0
for (i in rownames(no_cids_avail_man_cur)){
    print(paste0("Running query rowname ", i, " with cid ", no_cids_avail_man_cur[i,"cid"]))
    n_requests <- n_requests + 1
    getPcCand.trans(no_cids_avail_man_cur[i,"cid"])
    print(paste0(n_requests," completed."))
}
end.time <- Sys.time()
time.taken <- end.time - start.time
time.taken

###merge tps across all runs
merged_tp_suspect_list <- list.files(path=file.path(getwd(),"/output_TP_files_20200930_816_cns_mappedDashboard_1262_cid_synonyms"), pattern = "CID*", recursive = TRUE, full.names = TRUE) %>% 
lapply(read_csv) %>%  
bind_rows

dim(merged_tp_suspect_list)
#how many names ("successor") and successorids are unique?
length(unique(merged_tp_suspect_list$successorcid))
length(unique(merged_tp_suspect_list$successor))

length(unique(merged_tp_suspect_list$predecessor))
unique(merged_tp_suspect_list$predecessor)
unique(merged_tp_suspect_list$predecessorcid)

#how many parents were actually from cns list?
#need to compare with inputs because of the multilateral way pubchem stores transofmraiton info --> simply counting no. files is wrong
parents_not_from_cns <- setdiff(unique(merged_tp_suspect_list$predecessorcid),query_idex_cns_cids$cid) #elements in x not in y
length(parents_not_from_cns) #these are parents generated as a result of PubChem's storage of TP info

parents_not_from_cns

#no. parents originally from CNS
length(unique(merged_tp_suspect_list$predecessorcid))-length(parents_not_from_cns)

merged_tp_suspect_list[order(merged_tp_suspect_list$predecessor)[9:16],1:9]

# remove redundant rows - result of PubChem's internal 2-way P-TP mapping
rem2way_merged_tp_suspect_list <- merged_tp_suspect_list[!duplicated(merged_tp_suspect_list[,c('predecessor','successor')]),]
dim(rem2way_merged_tp_suspect_list)
length(unique(rem2way_merged_tp_suspect_list$successorcid))
length(unique(rem2way_merged_tp_suspect_list$successor))
#length(unique(rem2way_merged_tp_suspect_list$predecessorcid))

myvec <- rem2way_merged_tp_suspect_list$successor
myvec <- as.character(myvec)
sort(myvec)

myvec[duplicated(myvec, fromLast=TRUE)]

rem2way_merged_tp_suspect_list[rem2way_merged_tp_suspect_list$successor=="Valsartan acid",]

#3 rows which were removed (redundant)
merged_tp_suspect_list[duplicated(merged_tp_suspect_list[,c('predecessor','successor')]),] #those which were removed because of redundancy created by 2-way relationship

###analysis of parents (predecessors)
print(paste("Out of ",nrow(rem2way_merged_tp_suspect_list)," rows, there are ",length(unique(rem2way_merged_tp_suspect_list$predecessor))," unique parent compounds i.e. predecessors.")) 
parents_more_than_one_tp <- rem2way_merged_tp_suspect_list[duplicxxated(rem2way_merged_tp_suspect_list$predecessor,fromLast=TRUE)|duplicated(rem2way_merged_tp_suspect_list$predecessor),]
print(paste("Out of ",nrow(rem2way_merged_tp_suspect_list)," rows, there are ",length(unique(parents_more_than_one_tp$predecessor))," unique parent compounds with more than one TP.")) 

#plot
n_parents_more_than_one_tp <- parents_more_than_one_tp  %>% dplyr::group_by(predecessor) %>% dplyr::summarise(n = n())
ggplot(n_parents_more_than_one_tp,aes(n)) + geom_bar() + scale_y_continuous(breaks=seq(0, 12, 2)) + 
labs(title = "How Many Parents Have >1 TP? ", x="No. TPs", y="No. Parents") +
theme(plot.title = element_text(size=20,hjust=0.5)) + theme(text = element_text(size=16)) 

#see some examples
parents_more_than_one_tp[1:10,1:8]

###analysis of tps (successors)
tps_more_than_one_parent <- rem2way_merged_tp_suspect_list[duplicated(rem2way_merged_tp_suspect_list$successor,fromLast=TRUE)|duplicated(rem2way_merged_tp_suspect_list$successor),] #14
print(paste("There are ",length(unique(tps_more_than_one_parent$successor))," TPs with more than one parent: ", unique(tps_more_than_one_parent$successor))) #7

#plot
n_tps_more_than_one_parent <- tps_more_than_one_parent  %>% dplyr::group_by(successor) %>% dplyr::summarise(n = n())
ggplot(n_tps_more_than_one_parent,aes(n)) + geom_bar() + 
labs(title = "How Many TPs Have >1 Parent? ", x="No. Parents", y="No. TPs") +
theme(plot.title = element_text(size=20,hjust=0.5)) + theme(text = element_text(size=16))  #3 TPs have 2, 1 TP has 3 parents

tps_more_than_one_parent[order(tps_more_than_one_parent$successor),1:8]

#The CAS numbers (1 typo: Ranitidine-N-Oxide's CAS was corrected from 738557-20-2 to 73857-20-2) were converted to CIDs using the Identifier Exchange Service https://pubchem.ncbi.nlm.nih.gov/idexchange/idexchange.cgi and the following settings:

tpsanliker <- read.delim("anliker2020/940885460039938799_idexchange_cpd_name_cid.txt",sep="\t",header=FALSE)
colnames(tpsanliker) <- c("synonym","successorcid") #to match column for merging

#check for NAs in successorcid
tpsanliker[is.na(tpsanliker$successorcid),]

#manually add cid where NA in succesorcid (N4-Acetylsulfadimethoxine)
tpsanliker[tpsanliker$synonym=="N4-Acetylsulfadimethoxine","successorcid"] <- 473359  
dim(tpsanliker)

#how many synonyms and successorcids are unique?
sapply(tpsanliker, function(x) length(unique(x)))

tpsanliker[duplicated(tpsanliker$synonym)|duplicated(tpsanliker$synonym,fromLast=TRUE),]

#merge tpsanliker with our pubchem-mined TPs
tpsanliker_rem2way_merged_tp_suspect_list <- full_join(rem2way_merged_tp_suspect_list,tpsanliker,by="successorcid")
dim(tpsanliker_rem2way_merged_tp_suspect_list) #100x15, compared to CAS - 115x15
#preview the joined table
tpsanliker_rem2way_merged_tp_suspect_list[70:76,c(1:8,15)]

#check for duplicates amongst matches across both anliker and the PubChem-mined list (NA row duplicates don't count)
#tpsanliker_rem2way_merged_tp_suspect_list[duplicated(tpsanliker_rem2way_merged_tp_suspect_list[,c('predecessor','successor')])|
 #                                         duplicated(tpsanliker_rem2way_merged_tp_suspect_list[,c('predecessor','successor')],fromLast=TRUE),]

write.csv(tpsanliker_rem2way_merged_tp_suspect_list,"output/tpsanliker_rem2way_merged_tp_suspect_list.csv")

new_finaltps <- read.csv("input/tpsanliker_rem2way_merged_tp_suspect_list_man_cur.csv")
new_finaltps_to_suspect_screening <- new_finaltps[new_finaltps$to_suspect_screening==1,]
new_finaltps_to_suspect_screening$successor <- as.character(new_finaltps_to_suspect_screening$successor)

str(new_finaltps_to_suspect_screening)

sum(!is.na(new_finaltps_to_suspect_screening$synonym))

commontps <- new_finaltps_to_suspect_screening[!is.na(new_finaltps_to_suspect_screening$synonym) & !is.na(new_finaltps_to_suspect_screening$transformation),]
print(paste(length(unique(commontps$successor)),"unique TPs were common across PubChem-mined and Anliker's lists."))

pc_mined_not_in_tpsanliker <- paste(new_finaltps_to_suspect_screening[is.na(new_finaltps_to_suspect_screening$synonym),"successor"], collapse=", ")
print(paste(sum(is.na(new_finaltps_to_suspect_screening$synonym)),"TPs were in the Pubchem-mined list which were not in tpsanliker:",pc_mined_not_in_tpsanliker,"."))


pc_mined_not_in_tpsanliker

parents_pc_mined_not_in_tpsanliker <- new_finaltps_to_suspect_screening[is.na(new_finaltps_to_suspect_screening$synonym),]
print(paste("These TPs not in tpsanliker came from",length(unique(parents_pc_mined_not_in_tpsanliker$predecessor)),"unique parents."))

parents_pc_mined_not_in_tpsanliker$predecessor <- as.character(parents_pc_mined_not_in_tpsanliker$predecessor)
parents_pc_mined_not_in_tpsanliker$predecessor
#parents_pc_mined_not_in_tpsanliker[,order("predecessor")]


parents_pc_mined_not_in_tpsanliker[duplicated(parents_pc_mined_not_in_tpsanliker$predecessor,fromLast=TRUE)|duplicated(parents_pc_mined_not_in_tpsanliker$predecessor),]

#parents which have TP info in Transformations Section in PubChem
parents_noTPinfo <- data.frame(TP_info_avail_PubChem=c("yes", "no"),
                no_parents=c(28,816-28))
ggplot(data=parents_noTPinfo, aes(x=TP_info_avail_PubChem, y=no_parents)) +
  geom_bar(stat="identity") + labs(title = "How Many CNS Parents \n had TP Info in PubChem Transformations? \n (as of 10/2020)", x="", y="No. CNS Parents") +
  theme(plot.title = element_text(size=20,hjust=0.5)) + theme(text = element_text(size=16)) +
    theme(text = element_text(size=18)) 

tpsanliker_not_in_pc_mined <- paste(new_finaltps_to_suspect_screening[is.na(new_finaltps_to_suspect_screening$transformation),"synonym"], collapse=", ")
print(paste(sum(is.na(new_finaltps_to_suspect_screening$transformation)),"TPs were in the tpsanliker list which were not in Pubchem-mined:",tpsanliker_not_in_pc_mined,".")) 

new_finaltps$note1 <- as.character(new_finaltps$note1)
unique(new_finaltps$note1)
print(paste(nrow(new_finaltps[!is.na(new_finaltps$note1) & new_finaltps$note1=="redundant",])," TP compound CIDs were redundant because they were simply different stereochemical forms of the same compound."))
print(paste(nrow(new_finaltps[!is.na(new_finaltps$note1) & new_finaltps$note1=="parent_not_in_CNS",]) + nrow(new_finaltps[!is.na(new_finaltps$note1) & new_finaltps$note1=="parent_not_in_CNS_same_class",])," TP compounds have parent compounds (drugs) which are not in the CNS list (n=816)."))
print(paste(nrow(new_finaltps[!is.na(new_finaltps$note1) & new_finaltps$note1=="parent_in_CNS_TP_not_mapped",]), "TP compounds have parent compounds (drugs) which are in the CNS list (n=816), but their Parents-TP relationships are not mapped in PubChem."))
print(paste(nrow(new_finaltps[!is.na(new_finaltps$note1) & new_finaltps$note1=="wrongly_mapped_in_Pubchem",])," TP compound has a Parent-TP relationship which is 'wrongly' or 'incompletely' mapped in PubChem."))

new_finaltps[!is.na(new_finaltps$note1) & new_finaltps$note1=="parent_not_in_CNS","successor"]
new_finaltps[!is.na(new_finaltps$note1) & new_finaltps$note1=="parent_not_in_CNS_same_class","successor"]

new_finaltps[!is.na(new_finaltps$note1) & new_finaltps$note1=="parent_in_CNS_TP_not_mapped","synonym"]

new_finaltps[!is.na(new_finaltps$note1) & new_finaltps$note1=="wrongly_mapped_in_Pubchem","synonym"]

tail(new_finaltps_to_suspect_screening[,c(1:8,16:20)])

length(new_finaltps_to_suspect_screening$successor)
length(new_finaltps_to_suspect_screening$successorcid)

length(unique(new_finaltps_to_suspect_screening$successorcid))
length(unique(new_finaltps_to_suspect_screening$successor))

new_finaltps_more_than_one_parent <- new_finaltps_to_suspect_screening[duplicated(new_finaltps_to_suspect_screening$successorcid)|duplicated(new_finaltps_to_suspect_screening$successorcid,fromLast=TRUE),c(1:9,16:20),]
new_finaltps_more_than_one_parent[order(new_finaltps_more_than_one_parent$successor),]

#write out for idexchange, not including rows which are the same TP but have multiple parents
write.table(unique(new_finaltps_to_suspect_screening$successorcid),"pharma_CNS/anliker2020/tpsanliker_rem2way_merged_suspect_list_man_cur_cids_to_SMILES", row.names=FALSE, col.names=FALSE)
#submit this to ID exchange to get SMILES

#read in ID Exchange output giving SMILES for CIDs
id_exchange_output_unique_tpsanliker_rem2way_merged <- read.table('pharma_CNS/anliker2020/1715112478001380621_cid_SMILES_82.txt',comment.char = "")
colnames(id_exchange_output_unique_tpsanliker_rem2way_merged) <- c("successorcid","SMILES")
id_exchange_output_unique_tpsanliker_rem2way_merged$SMILES <- as.character(id_exchange_output_unique_tpsanliker_rem2way_merged$SMILES)
str(id_exchange_output_unique_tpsanliker_rem2way_merged)

#add compound names to cid-SMILES columns
withSMILES_id_exchange_output_unique_tpsanliker_rem2way_merged <- left_join(id_exchange_output_unique_tpsanliker_rem2way_merged,new_finaltps) #adds back more-than-one-parent rows
#remove extraneous columns
suspect_list <- withSMILES_id_exchange_output_unique_tpsanliker_rem2way_merged[,c("successorcid","successor","SMILES")]

str(suspect_list)
tail(suspect_list)

suspect_list[duplicated(suspect_list$successor)|duplicated(suspect_list$successor,fromLast=TRUE),] #new_finaltps_more_than_one_parent

suspect_list_out <- unique(suspect_list)

dim(suspect_list_out)

#write final TP suspect list containing anliker TPs + PubChem TPs
write.table(suspect_list_out, "output/20201002_cns_anliker_82_tps.csv", row.names=FALSE, quote=FALSE, sep="\t") 




