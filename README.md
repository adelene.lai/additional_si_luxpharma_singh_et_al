# Additional Supporting Information for the Manuscript:

> ## [Occurrence and Distribution of Pharmaceuticals and their Transformation Products in Luxembourgish Surface Waters](https://doi.org/10.26434/chemrxiv.14564301.v1) 									
>
> ### Randolph R. Singh<sup>1†\*</sup>, Adelene Lai<sup>1,2</sup>, Jessy Krier<sup>1</sup>, Todor Kondic<sup>1</sup>, Philippe Diderich<sup>3</sup>, Emma L. Schymanski<sup>1\*</sup>							
>
>
>
> <sup>1</sup> Luxembourg Centre for Systems Biomedicine (LCSB), University of Luxembourg, Luxembourg
<sup>2</sup>Institute for Inorganic and Analytical Chemistry, Friedrich-Schiller University, Lessing Strasse 8, 07743, Jena, Germany
<sup>3</sup> Water Management Agency, Ministry of the Environment, Climate and Sustainable Development, Luxembourg
<sup>†</sup> Current affiliation: IFREMER (Institut Français de Recherche pour l’Exploitation de la Mer), Laboratory of Biogeochemistry of Organic Contaminants, Rue de l’Ile d’Yeu, BP 21105,Nantes Cedex 3, 44311, France															
>
> \* Corresponding authors: RRS: randolph.singh@ifremer.fr and ELS: emma.schymanski@uni.lu 		


------
Main code contributors: Adelene Lai and Emma Schymanski

Please cite the code in this repository as:

Singh, Randolph, Lai, Adelene, Krier, Jessy, Kondic, Todor, Diderich, Philippe, & Schymanski, Emma. (2021, May 10). Additional Supporting Information to "Occurrence and Distribution of Pharmaceuticals and their Transformation Products in Luxembourgish Surface Waters" by Singh et al. (Version zenodo-v1). Zenodo. http://doi.org/10.5281/zenodo.4745360
------



This repository contains Additional Supporting Information to the above manuscript ([preprint](https://doi.org/10.26434/chemrxiv.14564301.v1)). It contains all code and inputs used as described in detail in the Methods section of the manuscript. It is organised as:


| Subdirectory   |   Description                                                                                |
| ---------------|:---------------------------------------------------------------------------------------------|
| `figures/`		 |visualise data and generate Figures 2, 3, 4, and 5                                            |
| `metfrag/`     |.R script to run MetFrag as described in Methods section                                      |
| `tps/`				 |generate a curated Suspect List of TPs mined from PubChem & merged with list in the literature|



Besides all necessary input files and code, all outputs generated have also been included for completeness.

In `tps/` and `figures/`, all code was originally written and run in Jupyter Notebooks (.ipynb). These Notebook files contain rich explanatory text written using Markdown that is interspersed between cells. Users are recommended to clone this repository and open the original Notebook files on their local machine, as GitLab has documented issues rendering Notebooks properly. However, those unable to open Notebooks locally can view them as rendered .html files which have been included in the subdirs. The code in the Notebooks has also been provided as .R files.
